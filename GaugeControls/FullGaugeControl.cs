﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace GaugeControls
{
    public partial class FullGaugeControl : UserControl
    {
        [Browsable(true)]
        [Category("Appearance")]
        public float Value { get; set; } = 10F;
        [Browsable(true)]
        [Category("Appearance")]
        public float Minimum { get; set; } = 10F;
        [Browsable(true)]
        [Category("Appearance")]
        public float Maximum { get; set; } = 20F;

        protected float _angleOffset = 190F; // tilt the clock circle to angle of 270 degree
        protected float _angleRange = 160F;
        protected float _arrowEndMultipiler = 2.4F;
        protected float _dotPosMultipiler = 2.2F;
        private float[] sin = new float[360]; // to store sine values for every andgle in a circle
        private float[] cos = new float[360]; // the same for cosine values

        public FullGaugeControl()
        {
            for (int i = 0; i < 360; i++)
            {
                sin[i] = (float)Math.Sin(i * Math.PI / 180.0F);
                cos[i] = (float)Math.Cos(i * Math.PI / 180.0F);
            }
            InitializeComponent();
        }

        protected virtual PointF SetCenter()
        {
            return new PointF(ClientSize.Width / 2.0F, ClientSize.Height - 10);
        }

        private void FullGaugeControl_Paint(object sender, PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            PointF center = SetCenter();
            PaintDots(graphics, center);
            PaintArrow(graphics, center);
        }

        private void PaintArrow(Graphics g, PointF center)
        {
                float arrowRadius = ClientSize.Width / _arrowEndMultipiler;
                int arrowAngle = (int)((Value - Minimum) * (_angleRange / (Maximum - Minimum)) + _angleOffset);
                PointF endPoint = new PointF(center.X + arrowRadius * cos[arrowAngle % 360], center.Y + arrowRadius * sin[arrowAngle % 360]);
                g.DrawLine(Pens.Red, center, endPoint);
        }
        private void PaintDots(Graphics g, PointF center)
        {
            for (float i = 0; i <= (Maximum - Minimum); i++)
            {
                float dotRadius = ClientSize.Width / _dotPosMultipiler;
                int dotAngle = (int)(i * (_angleRange / (Maximum - Minimum)) + _angleOffset);
                PointF endPoint = new PointF(center.X + dotRadius * cos[dotAngle % 360], center.Y + dotRadius * sin[dotAngle % 360]);
                g.FillEllipse(Brushes.Black, new RectangleF(endPoint, new SizeF(3F, 3F)));
            }
        }

        private void repainTimer_Tick(object sender, EventArgs e)
        {
            Invalidate();
        }
    }
}
