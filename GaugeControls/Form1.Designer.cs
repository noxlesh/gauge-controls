﻿namespace GaugeControls
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rightHalfGaugeControl1 = new GaugeControls.RightHalfGaugeControl();
            this.leftHalfGaugeControl1 = new GaugeControls.LeftHalfGaugeControl();
            this.fullGaugeControl1 = new GaugeControls.FullGaugeControl();
            this.SuspendLayout();
            // 
            // rightHalfGaugeControl1
            // 
            this.rightHalfGaugeControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.rightHalfGaugeControl1.Location = new System.Drawing.Point(574, 12);
            this.rightHalfGaugeControl1.Maximum = 22F;
            this.rightHalfGaugeControl1.Minimum = 0F;
            this.rightHalfGaugeControl1.Name = "rightHalfGaugeControl1";
            this.rightHalfGaugeControl1.Size = new System.Drawing.Size(154, 155);
            this.rightHalfGaugeControl1.TabIndex = 2;
            this.rightHalfGaugeControl1.Value = 17F;
            // 
            // leftHalfGaugeControl1
            // 
            this.leftHalfGaugeControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.leftHalfGaugeControl1.Location = new System.Drawing.Point(363, 12);
            this.leftHalfGaugeControl1.Maximum = 25F;
            this.leftHalfGaugeControl1.Minimum = 10F;
            this.leftHalfGaugeControl1.Name = "leftHalfGaugeControl1";
            this.leftHalfGaugeControl1.Size = new System.Drawing.Size(154, 155);
            this.leftHalfGaugeControl1.TabIndex = 1;
            this.leftHalfGaugeControl1.Value = 15F;
            // 
            // fullGaugeControl1
            // 
            this.fullGaugeControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fullGaugeControl1.Location = new System.Drawing.Point(12, 12);
            this.fullGaugeControl1.Maximum = 20F;
            this.fullGaugeControl1.Minimum = 10F;
            this.fullGaugeControl1.Name = "fullGaugeControl1";
            this.fullGaugeControl1.Size = new System.Drawing.Size(300, 155);
            this.fullGaugeControl1.TabIndex = 0;
            this.fullGaugeControl1.Value = 12F;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.rightHalfGaugeControl1);
            this.Controls.Add(this.leftHalfGaugeControl1);
            this.Controls.Add(this.fullGaugeControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private FullGaugeControl fullGaugeControl1;
        private LeftHalfGaugeControl leftHalfGaugeControl1;
        private RightHalfGaugeControl rightHalfGaugeControl1;
    }
}

