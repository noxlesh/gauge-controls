﻿namespace GaugeControls
{
    partial class FullGaugeControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.repainTimer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // repainTimer
            // 
            this.repainTimer.Enabled = true;
            this.repainTimer.Tick += new System.EventHandler(this.repainTimer_Tick);
            // 
            // FullGaugeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DoubleBuffered = true;
            this.Name = "FullGaugeControl";
            this.Size = new System.Drawing.Size(298, 148);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FullGaugeControl_Paint);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer repainTimer;
    }
}
