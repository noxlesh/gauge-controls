﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GaugeControls
{
    public partial class LeftHalfGaugeControl : FullGaugeControl
    {

        public LeftHalfGaugeControl()
        {
            InitializeComponent();
            Width = Width / 2;
            _angleOffset = 185F;
            _angleRange = 80F;
            _arrowEndMultipiler = 1.2F;
            _dotPosMultipiler = 1.1F;
    }

        protected override PointF SetCenter()
        {
            return new PointF(ClientSize.Width - 10, ClientSize.Height - 10);
        }
    }
}
